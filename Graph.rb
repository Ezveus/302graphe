class Graph
  def initialize tab
    @res = tab
    @bows = tab.count
    @h = {}
    @mAdjacence = []
    @res.each do |x|
      @h[x[0]] = 1
      @h[x[1]] = 1
    end
    @points = @h.keys.count
    i = 0
    @h.each do |x, _|
      @h[x] = i
      i += 1
    end
  end
  
  def nbBows
    @bows
  end
  
  def nbPoints
    @points
  end

  def adjacence
    m = Array.new(@points) {Array.new(@points, 0)}
    (0...@points).each do |x|
      (0...@points).each do |y|
        @res.each do |i, j|
          m[x][y] = 1 if @h[i] == x && @h[j] == y 
        end
      end
    end
    @mAdjacence = m
    (0...@points).each do |x|
      (0...@points).each do |y|
        printf "%d ", m[x][y]
      end
      puts ""
    end
  end
  
  def adjacenceG2
    m = Array.new(@points) {Array.new(@points, 0)}
    (0...@points).each do |x|
      (0...@points).each do |y|
        (0...@points).each do |k|
          m[x][y] += @mAdjacence[x][k] * @mAdjacence[k][y]
        end
      end
    end
    (0...@points).each do |x|
      (0...@points).each do |y|
        printf "%d ", m[x][y]
      end
      puts ""
    end
  end
  
  def access
    a = Array.new(@points) {Array.new(@points, 0)}
    (0...@points).each do |i|
      (0...@points).each do |j|
        if i == j
          a[i][i] = 1
        else
          a[i][j] = @mAdjacence[i][j]
        end
      end
    end
    (0...@points).each do |i|
      (0...@points).each do |j|
        (0...@points).each do |k|
          a[i][j] = a[i][j] | (a[i][k] & a[k][j])
        end
      end
    end
    (0...@points).each do |x|
      (0...@points).each do |y|
        printf "%d ", a[x][y]
      end
      puts ""
    end
  end
  
  def shortWay
    a = Array.new(@points) {Array.new(@points, 0)}
    (0...@points).each do |i|
      (0...@points).each do |j|
        if i == j
          a[i][j] = 0
        elsif @mAdjacence[i][j] == 0
          a[i][j] = 9001
        else
          a[i][j] = 1
        end
      end
    end
    (0...@points).each do |i|
      (0...@points).each do |j|
        (0...@points).each do |k|
          b = [a[i][j], a[i][k] + a[k][j]]
          a[i][j] = b.min
        end
      end
    end
    (0...@points).each do |x|
      (0...@points).each do |y|
        if a[x][y] == 9001
          printf "x "
        else
          printf "%d ", a[x][y]
        end
      end
      puts ""
    end
  end
end
