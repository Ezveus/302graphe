module Parser
  def self.parse path
    observ = File.new path
    nb_bows = observ.readline.to_i
    res = []
    observ.each_line do |line|
      useless, a, b = line.split(/ *(\w+) +(\w+)\n/)
      res << [a, b]
    end
    res
  end
end
